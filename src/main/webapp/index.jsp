<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Big Judd Burgers</title>
</head>
<body>
<h1><%= "Welcome to Big Judd Burgers!" %>
</h1>
<p>To start your order click <a href="${pageContext.request.contextPath}/order.html">here</a></p>
</body>
</html>