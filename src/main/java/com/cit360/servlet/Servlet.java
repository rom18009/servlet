package com.cit360.servlet;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;

// https://youtu.be/QZVosiLT-y8 - Brother Tuckett video to get tomcat and JavaEE project setup
@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    public Servlet() {}

        protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            String burger = request.getParameter("burger");
            String fries = request.getParameter("fries");
            String drink = request.getParameter("drink");
            String name = request.getParameter("name");
            try (PrintWriter out = response.getWriter()) {
                response.setContentType("text/html");
                out.println("<html><head><style>div{width:auto;background-color:#F0F0F0;padding:2px 8px;margin: 14px 0;border-radius:5px;}</style></head><body>");
                out.println("<h3>Thank you for your order " + name + "!" + "</h3>");
                out.println("<p>Your food will be out shortly.</p>");
                out.println("<div><p>Burger: " + burger + "</p>");
                out.println("<p>Fries: " + fries + "</p>");
                out.println("<p>Drink: " + drink + "</p></div>");
                out.println("<button onclick=\"goBack()\">Order again</button><script>function goBack(){window.history.back();}</script>");
                out.println("</body></html>");
            }
        }
        // https://www.jetbrains.com/help/idea/deploying-a-web-app-into-an-app-server-container.html#bd4f6ba6
        protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try (PrintWriter out = response.getWriter()) {
                response.setContentType("text/html");
                out.println("<html><head><style>div{width:auto;background-color:#F0F0F0;padding:2px 8px;margin: 14px 0;border-radius:5px;}</style></head><body>");
                out.println("<h3>Something went wrong</h3>");
                out.println("<div><p>This page cannot be accessed directly.</p></div>");
                out.println("<button onclick=\"goBack()\">Go Back</button><script>function goBack(){window.history.back();}</script>");
                out.println("</body></html>");
            }
        }
    public void destroy() {
    }
}